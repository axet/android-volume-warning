package com.github.axet.volumewarning.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.volumewarning.app.VolumeApplication;

public class OnBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        OptimizationPreferenceCompat.setBootInstallTime(context, VolumeApplication.PREFERENCE_BOOT, System.currentTimeMillis());
        VolumeService.startIfEnabled(context);
    }
}
