package com.github.axet.volumewarning.app;

import android.content.Context;
import android.content.Intent;

import com.github.axet.androidlibrary.app.MainApplication;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.volumewarning.services.VolumeService;

public class VolumeApplication extends MainApplication {

    public static String PREFERENCE_OPTIMIZATION = "optimization";
    public static String PREFERENCE_NEXT = "next";
    public static String PREFERENCE_BOOT = "boot";
    public static String PREFERENCE_INSTALL = "install";

    public NotificationChannelCompat channelStatus;

    public static VolumeApplication from(Context context) {
        return (VolumeApplication) MainApplication.from(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        channelStatus = new NotificationChannelCompat(this, "status", "Status", NotificationManagerCompat.IMPORTANCE_LOW);
    }
}
