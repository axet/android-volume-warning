package com.github.axet.volumewarning.services;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.volumewarning.R;
import com.github.axet.volumewarning.app.VolumeApplication;

public class VolumeService extends PersistentService {
    public static String TAG = VolumeService.class.getSimpleName();

    public static String[] PERMISSIONS = new String[]{"android.permission.WRITE_SECURE_SETTINGS"};

    public static int NOTIFICATION_ICON = 1;
    public static long PERIOD = AlarmManager.HOUR1;

    static {
        OptimizationPreferenceCompat.ICON = true;
    }

    {
        id = NOTIFICATION_ICON;
    }

    long next;

    public static void start(Context context) {
        start(context, new Intent(context, VolumeService.class));
    }

    public static boolean isSafe(Context context) {
        int id = context.getResources().getIdentifier("config_safe_media_volume_enabled", "bool", "android");
        return context.getResources().getBoolean(id);
    }

    public static void startIfEnabled(Context context) {
        if (!Storage.permitted(context, VolumeService.PERMISSIONS))
            return;
        if (!isSafe(context))
            return;
        if (!startIfPersistent(context, true, new Intent(context, VolumeService.class), VolumeApplication.PREFERENCE_OPTIMIZATION)) {
            updateGlobals(context);
            registerNext(context);
        }
    }

    public static void updateGlobals(Context context) {
        try {
            if (!Storage.permitted(context, VolumeService.PERMISSIONS))
                return;
            if (Build.VERSION.SDK_INT >= 17) {
                Settings.Global.putInt(context.getContentResolver(), "audio_safe_volume_state", 1);
                Settings.Secure.putInt(context.getContentResolver(), "unsafe_volume_music_active_ms", 300 * AlarmManager.HOUR1);
            }
        } catch (Exception e) {
            Toast.Error(context, e).show();
        }
    }

    public static void registerNext(Context context) {
        long time = System.currentTimeMillis() + PERIOD;
        AlarmManager.set(context, time, new Intent(context, VolumeService.class));
    }

    public VolumeService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onCreateOptimization() {
        optimization = new ServiceReceiver(VolumeApplication.PREFERENCE_OPTIMIZATION, VolumeApplication.PREFERENCE_NEXT);
        optimization.create();
    }

    @Override
    public void onRestartCommand() {
        super.onRestartCommand();
        registerNext();
    }

    @Override
    public void onStartCommand(Intent intent) {
        super.onStartCommand(intent);
        updateGlobals(this);
        registerNext();
    }

    public void registerNext() {
        long now = System.currentTimeMillis();
        if (next + PERIOD < now) {
            registerNext(this);
            next = now;
        }
        if (!isPersistent(this, true, optimization.key))
            stopSelf();
    }

    @Override
    public Notification build(Intent intent) {
        return new PersistentIconBuilder()
                .create(R.style.AppTheme, VolumeApplication.from(this).channelStatus)
                .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                .build();
    }
}
