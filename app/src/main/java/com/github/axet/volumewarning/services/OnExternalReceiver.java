package com.github.axet.volumewarning.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnExternalReceiver extends com.github.axet.androidlibrary.services.OnExternalReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        VolumeService.startIfEnabled(context);
    }
}
