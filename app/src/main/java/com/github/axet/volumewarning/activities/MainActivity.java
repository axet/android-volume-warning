package com.github.axet.volumewarning.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.preference.PreferenceViewHolder;
import android.support.v7.view.WindowCallbackWrapper;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.github.axet.androidlibrary.app.Storage;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.widgets.AppCompatThemeActivity;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.volumewarning.R;
import com.github.axet.volumewarning.app.VolumeApplication;
import com.github.axet.volumewarning.services.VolumeService;

public class MainActivity extends AppCompatThemeActivity {
    Handler handler = new Handler();
    Runnable run = new Runnable() {
        @Override
        public void run() {
            update();
        }
    };

    PersistentService.SettingsReceiver receiver;
    OptimizationPreferenceCompat icon;
    PreferenceViewHolder iconHolder;

    @Override
    public int getAppTheme() {
        return R.style.AppTheme;
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FrameLayout v = (FrameLayout) findViewById(R.id.optimization);
        Window w = getWindow();
        w.setCallback(new WindowCallbackWrapper(w.getCallback()) {
            @Override
            public void onWindowFocusChanged(boolean hasFocus) {
                super.onWindowFocusChanged(hasFocus);
                icon.onResume();
                icon.onBindViewHolder(iconHolder);
            }
        });
        icon = new OptimizationPreferenceCompat(OptimizationPreferenceCompat.themedContext(this));
        icon.setKey(VolumeApplication.PREFERENCE_OPTIMIZATION);
        icon.setTitle(R.string.optimization_system);
        icon.setSummary(R.string.optimization_system_summary);
        icon.onResume();
        iconHolder = OptimizationPreferenceCompat.inflate(icon, v);
        update();

        receiver = new PersistentService.SettingsReceiver(new Intent(this, VolumeService.class), VolumeApplication.PREFERENCE_OPTIMIZATION);
        receiver.register(this);

        if (OptimizationPreferenceCompat.needKillWarning(this, VolumeApplication.PREFERENCE_NEXT))
            OptimizationPreferenceCompat.buildKilledWarning(new ContextThemeWrapper(this, getAppTheme()), true, VolumeApplication.PREFERENCE_OPTIMIZATION).show();
        else if (OptimizationPreferenceCompat.needBootWarning(this, VolumeApplication.PREFERENCE_BOOT, VolumeApplication.PREFERENCE_INSTALL))
            OptimizationPreferenceCompat.buildBootWarning(this).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        receiver.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        icon.onResume();
        icon.onBindViewHolder(iconHolder);
    }

    void update() {
        View w = findViewById(R.id.warning);
        View t = findViewById(R.id.text);
        if (!Storage.permitted(this, VolumeService.PERMISSIONS)) {
            w.setVisibility(View.VISIBLE);
            t.setVisibility(View.GONE);
            handler.postDelayed(run, 1000);
        } else {
            w.setVisibility(View.GONE);
            t.setVisibility(View.VISIBLE);
            VolumeService.startIfEnabled(this);
        }
    }
}
