# Volume Warning

Android shows annoying Volume Warning dialog if you listening for music. Most of the time music is not even loud, but when dialog raised volume dropped below 50% until you close this dialog and you have to wake up screen to restore previous volume manually.

Android has two global settings:

  * audio_safe_volume_state
  * unsafe_volume_music_active_ms

'audio_safe_volume_state' can be following:

  * private static final int SAFE_MEDIA_VOLUME_NOT_CONFIGURED = 0;
  * private static final int SAFE_MEDIA_VOLUME_DISABLED = 1;
  * private static final int SAFE_MEDIA_VOLUME_INACTIVE = 2;  // confirmed
  * private static final int SAFE_MEDIA_VOLUME_ACTIVE = 3;  // unconfirmed

Theoritically, you can disable it, but one some devices after 'unsafe_volume_music_active_ms' passes 'audio_safe_volume_state' will be restored to original value. So, you have to have persistent service to reset value regulary.

# Manuall install

    # gradle installDebug
    #
    # adb shell pm grant com.github.axet.volumewarning android.permission.WRITE_SECURE_SETTINGS

## adb hacks

You do not need this app if following works on your device: try set value manually, if it persistent on your device:

    # adb shell settings put global audio_safe_volume_state 1

## init.d script

On Lingeage OS devices you have to enable init.d by creating sysinit service

    mount -o remount,rw /system
    
    cat << EOF > /system/etc/init/sysinit.rc
    service sysinit /system/bin/sysinit
        oneshot
        class late_start
        user root
        group root
        disabled
    EOF

    cat << EOF > /system/etc/init.d/20volumereset
    #!/system/bin/sh
    settings put global audio_safe_volume_state 1
    settings put secure unsafe_volume_music_active_ms 1080000000
    EOF

    chmod +x /system/etc/init.d/20volumereset
